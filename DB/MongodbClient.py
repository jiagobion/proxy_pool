# coding: utf-8
"""
-------------------------------------------------
   File Name：    MongodbClient.py
   Description :  封装mongodb操作
   Author :       JHao netAir
   date：          2017/3/3
-------------------------------------------------
   Change Activity:
                   2017/3/3:
                   2017/9/26:完成对mongodb的支持
-------------------------------------------------
"""
import json

__author__ = 'Maps netAir'

from pymongo import MongoClient


class MongodbClient(object):
    def __init__(self, name, host, port, **kwargs):
        self.name = name
        port = int(port)
        self.client = MongoClient(host, port, **kwargs)
        self.db = self.client[self.name]

    def change_table(self, name):
        self.name = name

    def get(self, proxy):
        data = self.db[self.name].find_one({'proxy': proxy})
        return data

    def put(self, proxy_obj):
        if not self.db[self.name].count_documents({'proxy': proxy_obj.proxy}):
            self.db[self.name].insert_one(json.loads(proxy_obj.info_json))

    def pop(self):
        data = list(self.db[self.name].aggregate([{'$sample': {'size': 1}}]))
        if data:
            data = data[0]
            value = data['proxy']
            self.delete(value)
            return data
        return None

    def delete(self, value):
        self.db[self.name].remove({'proxy': value})

    def get_all(self):
        return list(self.db[self.name].find())

    def clear(self):
        # self.client.drop_database('proxy')
        self.db.drop_collection(self.name)

    def delete_all(self):
        self.db[self.name].remove()

    def update(self, key, value):
        self.db[self.name].update({'proxy': key}, {'$inc': {'num': value}})

    def exists(self, key):
        return True if self.db[self.name].find_one({'proxy': key}) is not None else False

    def get_number(self):
        return self.db[self.name].count()


if __name__ == "__main__":
    db = MongodbClient('first', 'localhost', 27017)
    # db.put('127.0.0.1:1')
    # db2 = MongodbClient('second', 'localhost', 27017)
    # db2.put('127.0.0.1:2')
    print(db.pop())
