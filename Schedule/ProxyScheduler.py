# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     ProxyScheduler
   Description :
   Author :        JHao
   date：          2019/8/5
-------------------------------------------------
   Change Activity:
                   2019/8/5: ProxyScheduler
-------------------------------------------------
"""
__author__ = 'JHao'

import sys
from apscheduler.schedulers.blocking import BlockingScheduler

sys.path.append('../')

from Config.ConfigGetter import config
from Schedule import do_raw_proxy_check, do_useful_proxy_check
from Manager import ProxyManager
from Util import LogHandler


class DoFetchProxy(ProxyManager):
    """ fetch proxy"""

    def __init__(self):
        ProxyManager.__init__(self)
        self.log = LogHandler('fetch_proxy')

    def main(self):
        self.log.info("start fetch proxy")
        self.fetch()
        self.log.info("finish fetch proxy")


def raw_proxy_scheduler():
    DoFetchProxy().main()
    do_raw_proxy_check()


def useful_proxy_scheduler():
    do_useful_proxy_check()


def run_scheduler():
    raw_proxy_scheduler()
    useful_proxy_scheduler()

    scheduler_log = LogHandler("scheduler_log")
    scheduler = BlockingScheduler(logger=scheduler_log)

    scheduler.add_job(raw_proxy_scheduler, 'interval', id="raw_proxy_check", name="raw_proxy定时采集",
                      **config.raw_proxy_trigger_args)
    scheduler.add_job(useful_proxy_scheduler, 'interval', id="useful_proxy_check", name="useful_proxy定时检查",
                      **config.useful_proxy_trigger_args)

    scheduler.start()


if __name__ == '__main__':
    run_scheduler()
