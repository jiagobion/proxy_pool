# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     __init__.py.py  
   Description :  
   Author :       JHao
   date：          2016/12/3
-------------------------------------------------
   Change Activity:
                   2016/12/3: 
-------------------------------------------------
"""
__author__ = 'JHao'

from Schedule.RawProxyCheck import do_raw_proxy_check
from Schedule.UsefulProxyCheck import do_useful_proxy_check
